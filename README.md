# Awdce

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Assumptions
* Sorting sessions and tracks by start time
* IE < 11 not supported
* Assuming dataset/schema is complete
* Not sure how to determine Q&A link href in screenshot
* Listing all session speakers/bios in order of data retrieved
* Redirecting to first session in track for unexpected URL patterns
* No l10n, a11y