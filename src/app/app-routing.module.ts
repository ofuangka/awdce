import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'video-archive', loadChildren: './features/video-archive/video-archive.module#VideoArchiveModule' },
      { path: '**', pathMatch: 'full', redirectTo: 'video-archive' }
    ],
    data: {
      title: 'Video Archive'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
