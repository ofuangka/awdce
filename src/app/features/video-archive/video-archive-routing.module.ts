import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SessionViewComponent } from './track-view/session-view/session-view.component';
import { FirstSessionRedirectGuard } from './track-view/first-session-redirect.guard';
import { SelectedSessionResolver } from './track-view/session-view/selected-session.resolver';
import { TrackListResolver } from './track-view/track-list.resolver';
import { TrackViewComponent } from './track-view/track-view.component';

export interface SessionViewRouteParams {
  trackId?: string;
  sessionId?: string;
}

export interface VideoArchiveResolveData {
  tracks?: Track[];
}

export interface SessionViewResolveData {
  selectedTrack?: Track;
  trackSessions?: Session[];
  selectedSession?: Session;
}

const routes: Routes = [
  {
    path: 'tracks/:trackId',
    component: TrackViewComponent,
    children: [
      {
        path: 'sessions/:sessionId',
        component: SessionViewComponent,
        resolve: {
          selectedSession: SelectedSessionResolver
        }
      },
      { path: '**', pathMatch: 'full', canActivate: [FirstSessionRedirectGuard] }
    ],
    resolve: {
      tracks: TrackListResolver
    }
  },
  { path: '**', pathMatch: 'full', redirectTo: 'tracks/0' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoArchiveRoutingModule { }
