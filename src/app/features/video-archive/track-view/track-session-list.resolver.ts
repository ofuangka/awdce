import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/services/session/session.service';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()
export class TrackSessionListResolver implements Resolve<Session[]> {
    constructor(private sessionService: SessionService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<Session[]> {
        return this.sessionService.initialize()
            .pipe(
                map(() => this.sessionService.findSessionsByTrackId(route.paramMap.get('trackId')))
            );
    }
}
