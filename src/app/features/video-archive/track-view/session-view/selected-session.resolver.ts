import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()
export class SelectedSessionResolver implements Resolve<Session> {

    constructor(private sessionService: SessionService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Session> {
        return this.sessionService.initialize()
            .pipe(
                map(() => this.sessionService.findSessionById(route.paramMap.get('sessionId'), route.parent.paramMap.get('trackId')))
            );
    }
}
