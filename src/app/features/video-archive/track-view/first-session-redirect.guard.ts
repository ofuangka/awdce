import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/services/session/session.service';
import { mergeMap } from 'rxjs/operators';

@Injectable()
export class FirstSessionRedirectGuard implements CanActivate {

  constructor(private router: Router, private sessionService: SessionService) { }

  canActivate(next: ActivatedRouteSnapshot): Observable<boolean> {
    return this.sessionService.initialize()
      .pipe(
        mergeMap(() => {
          const trackId = next.parent.paramMap.get('trackId');
          const firstSession = this.sessionService.findFirstSessionUnderTrackId(trackId);
          if (firstSession) {
            return this.router.navigate([`video-archive/tracks/${trackId}/sessions/${firstSession.Id}`]);
          }
        })
      );
  }
}
