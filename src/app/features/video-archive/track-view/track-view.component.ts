import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'at-track-view',
  templateUrl: './track-view.component.html'
})
export class TrackViewComponent implements OnInit {

  tracks: Track[];
  selectedTrackId: string;
  selectedTrack: Track;
  selectedTrackSessions: Session[];

  constructor(private activatedRoute: ActivatedRoute, private sessionService: SessionService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      this.selectedTrackId = paramMap.get('trackId');
      this.selectedTrack = this.sessionService.findTrackById(this.selectedTrackId);
      this.selectedTrackSessions = this.sessionService.findSessionsByTrackId(this.selectedTrackId);
    });
    this.activatedRoute.data
      .subscribe(data => this.tracks = data.tracks);
  }
}
