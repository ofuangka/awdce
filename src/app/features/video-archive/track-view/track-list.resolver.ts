import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/services/session/session.service';
import { map } from 'rxjs/operators';

@Injectable()
export class TrackListResolver implements Resolve<Track[]> {
    constructor(private sessionService: SessionService) { }
    resolve(): Observable<Track[]> {
        return this.sessionService.initialize()
            .pipe(
                map(() => this.sessionService.tracks)
            );
    }
}
