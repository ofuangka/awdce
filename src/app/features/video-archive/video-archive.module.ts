import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoArchiveRoutingModule } from './video-archive-routing.module';
import { SessionViewComponent } from './track-view/session-view/session-view.component';
import { SelectedSessionResolver } from './track-view/session-view/selected-session.resolver';
import { TrackListResolver } from './track-view/track-list.resolver';
import { TrackSessionListResolver } from './track-view/track-session-list.resolver';
import { SharedModule } from 'src/app/shared/shared.module';
import { TrackViewComponent } from './track-view/track-view.component';
import { FirstSessionRedirectGuard } from './track-view/first-session-redirect.guard';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    VideoArchiveRoutingModule
  ],
  providers: [TrackListResolver, TrackSessionListResolver, SelectedSessionResolver, FirstSessionRedirectGuard],
  declarations: [SessionViewComponent, TrackViewComponent]
})
export class VideoArchiveModule { }
