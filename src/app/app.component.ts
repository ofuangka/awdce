import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, ActivatedRouteSnapshot } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'at-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  title: string;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => this.title = this.readTitle(this.activatedRoute.snapshot.root));
  }

  private readTitle(activatedRouteSnapshot: ActivatedRouteSnapshot): string {
    const myTitle = activatedRouteSnapshot.data.title;
    if (activatedRouteSnapshot.firstChild) {
      return this.readTitle(activatedRouteSnapshot.firstChild) || myTitle;
    }
    return myTitle;
  }

}
