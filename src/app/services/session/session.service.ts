import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';

const BASE_10 = 10;

/* TODO: confirm date format */
const DATE_FORMAT = 'MM/DD/YYYY hh:mm:ss A';

interface SessionRegistryEntry {
  track: Track;
  sessions: Session[];
}

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  /* the session service stores session data under an artifical key known as the trackId,
    which is the 0-indexed order in which distinct tracks appear in the data */

  /* the sessionRegistry is a mapping from trackId to array of sessions associated to that track */
  private sessionRegistry: SessionRegistryEntry[];

  get sessions(): Session[] {
    if (this.sessionRegistry) {
      return this.sessionRegistry.reduce((acc, registryEntry) => acc.concat(registryEntry.sessions), []);
    }
    return [];
  }

  get tracks(): Track[] {
    return this.sessionRegistry.map(registryEntry => registryEntry.track) || [];
  }

  constructor(private http: HttpClient) { }

  /**
   * Retrieves the session data, populates the session registry
   * and returns the complete set of session objects
   *
   * @param forceClear (optional) pass in true to clear existing data
   * @returns an boolean Observable indicating whether the data was retrieved from the server (true) or from cache (false)
   */
  initialize(forceClear?: boolean): Observable<boolean> {

    /* clear out previous session data if it exists */
    if (forceClear) {
      this.sessionRegistry = null;
    }

    if (!this.sessionRegistry) {
      this.sessionRegistry = [];
      return this.http.get<SessionResponse>(`${environment.sessionUriPrefix}/sessions.json`)
        .pipe(
          tap(response => {
            if (!response || typeof response.Items !== 'object' || !Array.isArray(response.Items)) {
              throw new Error(`Unexpected response when retrieving sessions: ${response}`);
            }
          }),
          map(response => response.Items),
          tap(sessions => this.loadSessionRegistry(sessions)),
          map(() => true)
        );
    } else {
      return of(false);
    }
  }

  /**
   * Retrieves a track by id, or null if the id does not exist in the registry
   * @param trackId the trackId to search by
   */
  findTrackById(trackId: string): Track {
    const registryEntry = this.findEntryByTrackId(trackId);
    if (registryEntry) {
      return registryEntry.track;
    }
    return null;
  }

  /**
   * Retrieves sessions by trackId. Returns empty array if the trackId does not exist in the registry
   * @param trackId The trackId to search by
   */
  findSessionsByTrackId(trackId: string): Session[] {
    const registryEntry = this.findEntryByTrackId(trackId);
    if (registryEntry) {
      return registryEntry.sessions;
    }
    return [];
  }

  /**
   * Retrieves a session by id if it exists in the registry, null otherwise
   * @param sessionId The sessionId to search by
   * @param trackId (optional) The trackId to search by.
   *  If the trackId is provided and the session exists under different trackId, null is returned
   */
  findSessionById(sessionId: string, trackId?: string): Session {
    if (trackId != null) {
      const registryEntry = this.findEntryByTrackId(trackId);
      if (registryEntry) {
        return registryEntry.sessions.find(session => session.Id === sessionId) || null;
      }
      return null;
    }
    return this.sessions.find(session => session.Id === sessionId) || null;
  }

  /**
   * Returns true if the trackId exists in the session registry
   * @param trackId the trackId to find
   */
  trackIdExists(trackId: string): boolean {
    return this.findTrackById(trackId) !== null;
  }

  /**
   * Returns the first session under the trackId provided, or null if the trackId does not eixst
   * @param trackId the trackId to retrieve session for
   */
  findFirstSessionUnderTrackId(trackId: string): Session {
    const registryEntry = this.findEntryByTrackId(trackId);
    return (registryEntry && registryEntry.sessions[0]) || null;
  }

  /**
   * Returns true if the provided sessionId exists under the trackId provided
   * @param sessionId The sessionId
   * @param trackId The trackId
   */
  sessionIdExistsUnderTrackId(sessionId: string, trackId: string): boolean {
    return this.findSessionById(sessionId, trackId) != null;
  }

  private loadSessionRegistry(sessions: Session[]): void {

    sessions

      /* TODO: confirm any ordering of tracks/sessions and date format */
      .sort((a, b) => moment(a.TimeSlot.StartTime, DATE_FORMAT).valueOf() - moment(b.TimeSlot.StartTime, DATE_FORMAT).valueOf())
      .forEach(session => {
        const sessionTrack = session.Track;
        let registryEntry = this.findEntryByTrackTitle(sessionTrack.Title);

        /* if there isn't already an existing entry, create one and add it */
        if (!registryEntry) {
          registryEntry = {
            track: sessionTrack,
            sessions: []
          };
          this.sessionRegistry.push(registryEntry);
        }
        registryEntry.sessions.push(session);
      });
  }

  private findEntryByTrackTitle(title: string): SessionRegistryEntry {
    return this.sessionRegistry.find(entry => entry.track.Title === title) || null;
  }

  private findEntryByTrackId(trackId: string): SessionRegistryEntry {
    const trackIndex = parseInt(trackId, BASE_10);
    if (!isNaN(trackIndex) && trackIndex < this.sessionRegistry.length) {
      return this.sessionRegistry[trackIndex];
    }
    return null;
  }
}
