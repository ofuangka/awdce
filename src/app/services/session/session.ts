interface SessionResponse {
  Items: Session[];
}

interface Session {
  Room?: Room;
  Id: string;
  Description: string;
  IsFeatured: string;
  TimeSlot: TimeSlot;
  Track: Track;
  LastModified: string;
  Published: string;
  SessionType: SessionType;
  MetadataValues?: MetadataValue[];
  Title: string;
  Speakers?: Speaker[];
  Mandatory: string;
  Links?: string;
}

interface Speaker {
  AttendeeID: string;
  Biography?: string;
  LastName: string;
  Interests?: string;
  EmailAddress: string;
  Title?: string;
  AvailableforMeeting: string;
  FirstName: string;
  Industry?: string;
  Roles: string[];
  Website?: string;
  Twitter?: string;
  LinkedIn?: string;
  Blog?: string;
  Company: string;
  PhotoLink?: string;
  LastUpdated: string;
  ID: string;
  Facebook?: string;
}

interface MetadataValue {
  Value: string;
  Details: Details;
}

interface Details {
  Title: string;
  Options: string[];
  FieldType: string;
}

interface SessionType {
  Name: string;
}

interface Track {
  Title: string;
  Description: string;
}

interface TimeSlot {
  EndTime: string;
  Label: string;
  StartTime: string;
}

interface Room {
  Name: string;
  Capacity: string;
}
