import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpeakerPipe } from './speaker.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    SpeakerPipe
  ],
  declarations: [SpeakerPipe]
})
export class SharedModule { }
