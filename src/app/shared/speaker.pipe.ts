import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';

const COMPONENT_CLASSES = ['at-full-name', 'at-company'];

/**
 * Formats a speaker object to safe html
 */
@Pipe({
  name: 'speaker'
})
export class SpeakerPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(speaker: Speaker): SafeHtml {

    /* generate the full-name */
    const fullName = [speaker.FirstName, speaker.LastName]
      .filter(component => component != null)
      .join(' ');

    /* sanitize the components */
    return this.sanitizer.bypassSecurityTrustHtml([fullName, speaker.Company]
      .map((component, index) => {
        if (component != null) {
          return `<span class="${COMPONENT_CLASSES[index]}">${this.sanitizer.sanitize(SecurityContext.HTML, component)}</span>`;
        }
        return component;
      })
      .filter(component => component != null)
      .join(', '));
  }

}
